class WebRTCManager {
    constructor() {
        this.localStream = null;
        this.peerConnection = null;
        this.messages = [];
        this.setupSocket();
        this.setupUI();
    }

    setupSocket() {
        this.socket = io();
        this.socket.on('chatMessage', (message) => this.handleChatMessage(message));
    }

    setupUI() {
        this.startButton = document.getElementById('startButton');
        this.stopButton = document.getElementById('stopButton');
        this.shareScreenButton = document.getElementById('shareScreenButton');
        this.stopScreenSharingButton = document.getElementById('stopScreenSharingButton');
        this.localVideo = document.getElementById('localVideo');
        this.remoteVideo = document.getElementById('remoteVideo');
        this.chatForm = document.getElementById('chatForm');
        this.chatInput = document.getElementById('chatInput');
        this.chatMessages = document.getElementById('chatMessages');

        this.startButton.addEventListener('click', () => this.startWebRTC());
        this.stopButton.addEventListener('click', () => this.stopWebRTC());
        this.shareScreenButton.addEventListener('click', () => this.startScreenSharing());
        this.stopScreenSharingButton.addEventListener('click', () => this.stopScreenSharing());
        this.chatForm.addEventListener('submit', (e) => this.handleChatFormSubmit(e));
    }

    async startWebRTC() {
        try {
            this.localStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
            this.localVideo.srcObject = this.localStream;
            this.peerConnection = new RTCPeerConnection();
            this.localStream.getTracks().forEach(track => this.peerConnection.addTrack(track, this.localStream));
            this.peerConnection.ontrack = event => this.remoteVideo.srcObject = event.streams[0];
            this.startButton.disabled = true;
            this.stopButton.disabled = false;
        } catch (error) {
            console.error('Error accessing media devices:', error);
        }
    }

    stopWebRTC() {
        this.localStream.getTracks().forEach(track => track.stop());
        this.peerConnection.close();
        this.localVideo.srcObject = null;
        this.remoteVideo.srcObject = null;
        this.startButton.disabled = false;
        this.stopButton.disabled = true;
    }

    async startScreenSharing() {
        try {
            const screenStream = await navigator.mediaDevices.getDisplayMedia({ video: true, audio: true });
            this.stopLocalVideo();
            this.localVideo.srcObject = screenStream;
            this.localStream = screenStream;
            this.stopScreenSharingButton.disabled = false;
            this.shareScreenButton.disabled = true;
        } catch (error) {
            this.handleScreenSharingError(error);
        }
    }

    stopScreenSharing() {
        this.stopLocalVideo();
        this.localStream.getTracks().forEach(track => this.peerConnection.addTrack(track, this.localStream));
        this.shareScreenButton.disabled = false;
        this.stopScreenSharingButton.disabled = true;
    }

    stopLocalVideo() {
        this.localStream.getTracks().forEach(track => track.stop());
    }

    handleChatFormSubmit(event) {
        event.preventDefault();
        const message = this.chatInput.value.trim();
        if (message !== '') {
            this.sendMessage(message);
            this.chatInput.value = '';
        }
    }

    sendMessage(message) {
        const timestamp = new Date().toLocaleTimeString();
        const formattedMessage = `[${timestamp}] You: ${message}`;
        this.messages.push(formattedMessage);
        this.displayMessage(formattedMessage);
        this.socket.emit('chatMessage', formattedMessage);
    }

    handleChatMessage(message) {
        this.messages.push(message);
        this.displayMessage(message);
    }

    displayMessage(message) {
        const listItem = document.createElement('li');
        listItem.textContent = this.processMessage(message);
        this.chatMessages.appendChild(listItem);
    }

    processMessage(message) {
        return message;
    }

    handleScreenSharingError(error) {
        console.error('Error starting screen sharing:', error);
        if (error.name === 'NotAllowedError') {
            alert('Permission to access screen sharing was denied.');
        } else if (error.name === 'NotFoundError') {
            alert('Screen sharing is not supported in this environment.');
        } else {
            alert('Error starting screen sharing. Please make sure you have granted the necessary permissions.');
        }
    }
}

const webRTCManager = new WebRTCManager();
