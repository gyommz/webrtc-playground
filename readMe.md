# WebRTC Playground

This project is a simple WebRTC demo showcasing real-time communication between clients using WebRTC and Socket.IO. The application allows users to start and stop WebRTC sessions, share their screens, and engage in real-time chat.

## Features

- **WebRTC Video Streaming:** Users can start and stop WebRTC sessions, enabling real-time video streaming between clients.
- **Screen Sharing:** The application supports screen sharing, allowing users to share their screens during WebRTC sessions.
- **Real-time Chat:** Users can engage in real-time chat during WebRTC sessions.

## Dependencies

- **Express:** A minimal web application framework for Node.js.
- **Socket.IO:** Enables real-time, bidirectional, and event-based communication.
- **RTCPeerConnection:** Part of the WebRTC API for creating peer-to-peer connections.

## Folder Structure

- **public:** Contains static files served by Express, such as HTML, CSS, and client-side JavaScript.
- **src:** Contains server-side JavaScript files.

## Getting Started

- Clone the repository:

```bash
git clone https://github.com/your-username/webrtc-demo.git
```

- Install dependencies:


```bash
cd webrtc-demo
npm install
```

- Start the server:

```bash
npm start
```

- Open your web browser and go to http://localhost:3000.
