const express = require('express');
const http = require('http');
const socketIO = require('socket.io');

class WebRTCServer {
    constructor() {
        this.app = express();
        this.server = http.createServer(this.app);
        this.io = socketIO(this.server);
        this.setupExpress();
        this.setupSocket();
    }

    setupExpress() {
        this.app.use(express.static('public'));
        this.app.get('/', (req, res) => {
            res.sendFile(__dirname + '/index.html');
        });
        const PORT = process.env.PORT || 3000;
        this.server.listen(PORT, () => {
            console.log(`Server running on http://localhost:${PORT}`);
        });
    }

    setupSocket() {
        this.io.on('connection', (socket) => {
            console.log('A user connected');
            socket.on('disconnect', () => {
                console.log('User disconnected');
            });
            socket.on('startWebRTC', () => {
                this.startWebRTC(socket);
            });
            socket.on('stopWebRTC', () => {
                this.stopWebRTC();
            });
        });
    }

    async startWebRTC(socket) {
        try {
            const mediaStream = await this.getMediaStream();
            const peerConnection = this.createPeerConnection();

            mediaStream.getTracks().forEach(track => peerConnection.addTrack(track, mediaStream));

            peerConnection.ontrack = event => {
                this.io.emit('webRTCRemoteTrack', event.streams[0]);
            };

            socket.emit('webRTCStarted');
        } catch (error) {
            console.error('Error accessing media devices:', error);
        }
    }

    stopWebRTC() {
        // Notify all connected clients that WebRTC has stopped
        this.io.emit('webRTCStopped');
    }

    async getMediaStream() {
        return await navigator.mediaDevices.getDisplayMedia({ video: true, audio: true });
    }

    createPeerConnection() {
        return new RTCPeerConnection();
    }
}

const webRTCServer = new WebRTCServer();
